<?php


namespace App\Middleware;


use App\Exceptions\NoSuchObjectException;
use App\Services\UserService;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AuthMiddleware
{
    protected $container;

    /**
     * AuthMiddleware constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Example middleware invokable class
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param callable $next Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     *
     */
    public function __invoke(RequestInterface $request,ResponseInterface $response, $next)
    {
        if (isset($_COOKIE['user_id']) and isset($_COOKIE['token']) and !isset($_SESSION['user'])){
            /** @var UserService $userService */
            $userService = $this->container['UserService'];
            try {
                $userBySession = $userService->UserBySession($_COOKIE['user_id'], $_COOKIE['token']);
            } catch (NoSuchObjectException $e) {
                return $response->withRedirect('/logout',302);
            }
            $_SESSION['user'] = serialize($userBySession);
            setcookie('user_id', $_COOKIE['user_id'], time() + 2592000);
            setcookie('token', $_COOKIE['token'], time() + 2592000);
        }
        if(isset($_SESSION['user'])){
            return $next($request, $response);
        }
        return $response->withRedirect('/login', 302);
    }


}