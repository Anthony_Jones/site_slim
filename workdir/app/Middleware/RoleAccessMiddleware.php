<?php


namespace App\Middleware;


class RoleAccessMiddleware
{
    public function __invoke($request, $response, $next)
    {
        $user = unserialize($_SESSION['user']);
        if($user->getIsPrivileged() == 1) {
            return $next($request, $response);
        } else {
            return $response->withRedirect('/profile', 302);
        }
    }

}