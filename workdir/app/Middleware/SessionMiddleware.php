<?php


namespace App\Middleware;


use App\Exceptions\DataAccessException;
use App\Models\Session;

class SessionMiddleware
{
    protected $container;

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * SessionMiddleware constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Example middleware invokable class
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param callable $next Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke($request, $response, $next)
    {

        if($this->sessionIsSet() ) {
            setcookie("CWSession",$_COOKIE['CWSession'],time()+ 2592000);
        } else {
            $session = new Session();
            $session->setIp($this->get_client_ip());
            $session->setUserAgent($_SERVER['HTTP_USER_AGENT']);
            $token = (string)bin2hex(random_bytes(16));
            try {
                $this->getContainer()['SessionDAO']->create($session);
            } catch(DataAccessException $e) {
                echo 'Session was not start';
            }
            $id = $this->getContainer()['SessionDAO']->getByIp($this->get_client_ip())->getId();
            setcookie("CWSession",$id,time()+ 2592000);
        }
        $response = $next($request, $response);
        return $response;
    }

    public function sessionIsSet() {
        return isset($_COOKIE['CWSession']);
    }

    function get_client_ip() {
        $ipAddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipAddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipAddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipAddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipAddress = 'UNKNOWN';
        return $ipAddress;
    }
}