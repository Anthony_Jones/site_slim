<?php


namespace App\DAO;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Models\Model;
use App\Models\ResetToken;
use PDO;

class ResetTokenDAO implements DAO
{

    protected $pdo;

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }


    public function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;
    }

    /**
     * @param ResetToken $model
     * @throws DataAccessException
     */
    public function create(Model $model)
    {
        $con = $this->getPdo();
        $token = $model->getResetToken();
        $user_id = $model->getUserId();
        $date = $model->getExpiringDate();
        $query = "insert into reset_token (user_id, reset_token,expiring_date) VALUES (:user_id, :token, :date)";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':user_id' => $user_id,
            ':token' => $token,
            ':date' => $date
        ));
        if ($flag == false) {
            throw new DataAccessException("Token was not created");
        }
    }

    /**
     * @param ResetToken $model
     */
    public function update(Model $model)
    {
        $con = $this->getPdo();
        $token = $model->getResetToken();
        $user_id = $model->getUserId();
        $id = $model->getId();
        $query = "update reset_token set user_id=:user_id, reset_token=:token where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':user_id' => $user_id,
            ':token' => $token,
            ':id' => $id
        ));
        if ($flag == false) {
            throw new DataAccessException("Token was not created");
        }
    }

    /**
     * @param $user_id
     * @return ResetToken
     * @throws NoSuchObjectException
     */
    public function getByUserId($user_id)
    {
        $confirm = new ResetToken();
        $con = $this->getPDO();
        $query = "select id, reset_token from reset_token where user_id = :user_id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':user_id' => $user_id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if ($array == false) {
            throw new NoSuchObjectException("There is no such ResetToken");
        }
        $id = $array['id'];
        $token = $array['reset_token'];
        $confirm->setId($id);
        $confirm->setUserId($user_id);
        $confirm->setResetToken($token);
        return $confirm;
    }

    public function getByToken($token)
    {
        $confirm = new ResetToken();
        $con = $this->getPDO();
        $query = "select id, user_id, expiring_date from reset_token where reset_token = :token";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':token' => $token
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if ($array == false) {
            throw new NoSuchObjectException("There is no such ResetToken");
        }
        $id = $array['id'];
        $user_id = $array['user_id'];
        $expiring_date = $array['expiring_date'];
        $confirm->setId($id);
        $confirm->setUserId($user_id);
        $confirm->setResetToken($token);
        $confirm->setExpiringDate($expiring_date);
        return $confirm;
    }
    /**
     * @param $id
     * @return ResetToken
     * @throws NoSuchObjectException
     */
    public function getById($id)
    {
        $confirm = new ResetToken();
        $con = $this->getPDO();
        $query = "select user_id, reset_token from reset_token where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if ($array == false) {
            throw new NoSuchObjectException("There is no such ResetToken");
        }
        $user_id = $array['user_id'];
        $token = $array['reset_token'];
        $confirm->setId($id);
        $confirm->setUserId($user_id);
        $confirm->setResetToken($token);
        return $confirm;
    }

    /**
     * @param $model
     * @return bool
     * @throws DataAccessException
     */
    public function hasToken(ResetToken $model): bool
    {
        $user_id = $model->getUserId();
        $con = $this->getPDO();
        $query = "select count(*) as hasEmail from reset_token where user_id = :user_id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':user_id' => $user_id
        ));
        $result = $preparedQuery->fetch();
        if ($result == false) {
            throw new DataAccessException("Token wasn't counted ");
        }
        if ($result['hasEmail'] == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @param $id
     * @return ResetToken
     * @throws NoSuchObjectException
     */
    public function read($id)
    {
        return $this->getById($id);
    }

    /**
     * @param $id
     * @throws NoSuchObjectException
     * @throws DataAccessException
     */
    public function delete($id)
    {
        $reset_token = $this->read($id);
        $con = $this->getPdo();
        $query = "delete from reset_token where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        if ($flag == false) {
            throw new DataAccessException("Token wasn't deleted ");
        }


    }
}