<?php


namespace App\DAO;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Models\EmailConfirm;
use App\Models\Model;
use PDO;

class EmailConfirmDAO implements DAO
{

    protected $pdo;
    public function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;
    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    /**
     * @param EmailConfirm $model
     * @throws NoSuchObjectException
     * @throws DataAccessException
     */
    public function create(Model $model)
    {
        $emailConf = $model;
        try {
            $conf = $this->getByUserId($model->getUserId());
        } catch (NoSuchObjectException $e) {
            $user_id = $model->getUserId();
            $hash_key = $model->getHashKey();
            $con = $this->getPDO();
            $query = "insert into email_confirmation (user_id,hash_key) values (:user_id, :hash_key)";
            $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $flag = $preparedQuery->execute(array(
                ':user_id' => $user_id ,
                ':hash_key' => $hash_key
            ));
            if($flag == false ) {
                throw new DataAccessException('Confirmation was not create');
            }
        }
    }


    /**
     * @param $id
     * @return EmailConfirm
     * @throws NoSuchObjectException
     */
    public function getById($id) {
        $confirm = new EmailConfirm();
        $con = $this->getPDO();
        $query = "select user_id, hash_key from email_confirmation where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
       $preparedQuery->execute(array(
            ':id' => $id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if($array == false) {
            throw new NoSuchObjectException("There is no such EmailConfirmation");
        }
        $user_id = $array['user_id'];
        $key = $array['hash_key'];
        $confirm->setId($id);
        $confirm->setUserId($user_id);
        $confirm->setHashKey($key);
        return $confirm;
    }

    /**
     * @param $user_id
     * @return EmailConfirm
     * @throws NoSuchObjectException
     */
    public function getByUserId($user_id) {
        $confirm = new EmailConfirm();
        $con = $this->getPDO();
        $query = "select id, hash_key from email_confirmation where user_id = :user_id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':user_id' => $user_id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if($array == false) {
            throw new NoSuchObjectException("There is no such EmailConfirmation");
        }
        $id = $array['id'];
        $key = $array['hash_key'];
        $confirm->setId($id);
        $confirm->setUserId($user_id);
        $confirm->setHashKey($key);
        return $confirm;
    }

    /**
     * @param $specification
     * @return EmailConfirm
     * @throws NoSuchObjectException
     */
    public function read($specification)
    {
        return $this->getById($specification);
    }

    /**
     * @param $id
     * @throws NoSuchObjectException
     * @throws DataAccessException
     */
    public function delete($id)
    {
        $confrim = $this->read($id);
        $con = $this->getPdo();
        $query = "delete from email_confirmation where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        if($flag == false){
            throw new DataAccessException("Confirmation wasn't deleted");
        }
    }

}