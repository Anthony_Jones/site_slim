<?php


namespace App\DAO;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Exceptions\UniqueEmailException;
use App\Exceptions\UniqueNicknameException;
use App\Models\Model;
use App\Models\User;
use PDO;


class UserDAO implements DAO
{

    protected $pdo;

    /**
     * @param User $model
     * @throws DataAccessException if query wasn't executed right
     * @throws UniqueEmailException Email has to be unique
     * @throws UniqueNicknameException Nickname has to be unique
     */
    public function create(Model $model)
    {
        $email = $model->getEmail();
        $nickname = $model->getNickname();
        if ($this->hasEmail($model)) {
            throw new UniqueEmailException('Email has to be unique');
        }
        if ($this->hasNickname($model)) {
            throw new UniqueNicknameException('Nickname has to be unique');
        }
        $password = $model->getPassword();
        $con = $this->getPDO();
        $query = "insert into user (nickname,email,password) values (:nickname, :email, :password)";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':nickname' => $nickname,
            ':email' => $email,
            ':password' => $password,
        ));
        if ($flag == false) {
            throw new DataAccessException("User was not create");
        }
    }

    /**
     * @param User $model
     * @return bool
     * @throws DataAccessException if query wasn't executed right
     */
    public function hasNickname(Model $model)
    {
        $nickname = $model->getNickname();
        $con = $this->getPDO();
        $query = "select count(*) as hasNickname from user where nickname = :nickname";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':nickname' => $nickname
        ));
        if ($flag == false) {
            throw new DataAccessException("Nickname wasn't counted ");
        }
        $result = $preparedQuery->fetch();
        if ($result['hasNickname'] == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @param $nickname
     * @return User
     * @throws NoSuchObjectException
     */
    public function findByNickname($nickname)
    {
        $user = new User();
        $con = $this->getPDO();
        $query = "select id ,email, password , email_verified_at, is_privileged from user where nickname = :nickname";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':nickname' => $nickname
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if ($array == false) {
            throw new NoSuchObjectException("There is no such User");
        }
        $id = $array['id'];
        $email = $array['email'];
        $password = $array['password'];
        $email_verified_at = $array['email_verified_at'];
        $is_privileged = $array['is_privileged'];
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setNickname($nickname);
        $user->setId($id);
        $user->setEmailVerifiedAt($email_verified_at);
        $user->setIsPrivileged($is_privileged);
        return $user;
    }

    /**
     * @param $email
     * @return User
     * @throws NoSuchObjectException
     */
    public function findByEmail($email)
    {
        $user = new User();
        $con = $this->getPDO();
        $query = "select id ,nickname, password, email_verified_at, is_privileged from user where email = :email";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':email' => $email
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if ($array == false) {
            throw new NoSuchObjectException("There is no such User");
        }
        $id = $array['id'];
        $nickname = $array['nickname'];
        $password = $array['password'];
        $email_verified_at = $array['email_verified_at'];
        $is_privileged = $array['is_privileged'];
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setNickname($nickname);
        $user->setId($id);
        $user->setEmailVerifiedAt($email_verified_at);
        $user->setIsPrivileged($is_privileged);
        return $user;
    }

    /**
     * @param Model $model
     * @return bool Is there such email is Database
     * @throws DataAccessException if query wasn't executed right
     */
    public function hasEmail(Model $model)
    {
        $email = $model->getEmail();
        $con = $this->getPDO();
        $query = "select count(*) as hasEmail from user where email = :email";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':email' => $email
        ));
        $result = $preparedQuery->fetch();
        if ($result == false) {
            throw new DataAccessException("Email wasn't counted ");
        }
        if ($result['hasEmail'] == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @param User $model
     * @throws DataAccessException if query wasn't executed right
     */
    public function update(Model $model)
    {
        $id = $model->getId();
        $email = $model->getEmail();
        $nickname = $model->getNickname();
        $password = $model->getPassword();
        $email_verified_at = $model->getEmailVerifiedAt();
        $is_privileged = $model->getIsPrivileged();
        $con = $this->getPDO();
        $query = "update user set nickname =:nickname ,email = :email,password = :pass,
                email_verified_at = :email_verified_at, is_privileged = :is_privileged where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':nickname' => $nickname,
            ':email' => $email,
            ':pass' => $password,
            ':id' => $id,
            ':email_verified_at' => $email_verified_at,
            ':is_privileged' => $is_privileged
        ));
        if ($flag == false) {
            throw new DataAccessException("User wasn't updated");
        }
    }

    /**
     * @param $id
     * @return User from table user in database with such id
     * @throws NoSuchObjectException if there is not User with such id in table user from database
     */
    public function getById($id): User
    {
        $user = new User();
        $con = $this->getPDO();
        $query = "select email, nickname, password, email_verified_at, is_privileged from user where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        if ($flag == false) {
            throw new NoSuchObjectException("There is no such User");
        }
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        $email = $array['email'];
        $nickname = $array['nickname'];
        $password = $array['password'];
        $email_verified_at = $array['email_verified_at'];
        $is_privileged = $array['is_privileged'];
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setNickname($nickname);
        $user->setId($id);
        $user->setEmailVerifiedAt($email_verified_at);
        $user->setIsPrivileged($is_privileged);
        return $user;


    }

    /**
     * @param $specification array: ('nickname' => value, 'id' => value)
     * @return User
     * @throws NoSuchObjectException if there is not User with such id in table user from database
     */
    public function read($id)
    {
        return $this->getById($id);
    }

    /**
     * @param $id
     * @throws DataAccessException if query wasn't executed right
     * @throws NoSuchObjectException if there is not User with such id in table user from database
     */
    public function delete($id)
    {
        $user = $this->read($id);
        $con = $this->getPDO();
        $query = "delete from user where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        if ($flag == false) {
            throw new DataAccessException("User wasn't deleted");
        }
    }

    /**
     * Method decides with method call Update or Create
     * @param User $model
     * @throws DataAccessException if query wasn't executed right
     * @throws UniqueEmailException Email has to be unique
     * @throws UniqueNicknameException Nickname has to be unique
     */
    public function save(User $model)
    {
        if ($model->getId() != null) {
            $this->update($model);
        } else {
            $this->create($model);
        }
    }

    /**
     * UserDAO constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return PDO
     */
    public function getPDO(): PDO
    {
        return $this->pdo;
    }

}