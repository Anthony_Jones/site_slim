<?php


namespace App\DAO;

use PDO;

use App\Models\Model;

interface DAO
{
    public function __construct(PDO $PDO);

    /**
     * @param Model $model
     */
    public function create(Model $model);

    public function update(Model $model);

    public function read($specification);

    public function delete($id);
}