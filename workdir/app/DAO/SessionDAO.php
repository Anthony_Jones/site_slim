<?php


namespace App\DAO;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Models\Model;
use App\Models\Session;
use PDO;


class SessionDAO implements DAO
{
    protected $pdo;

    /**
     * @param Session $model
     * @throws DataAccessException
     */
    public function create(Model $model)
    {
        $ip = $model->getIp();
        $user_agent = $model->getUserAgent();
        $con = $this->getPDO();
        $query = "insert into session (ip,user_agent) values(hex(INET_ATON(:ip)), :user_agent)";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
           ':ip' => $ip,
            ':user_agent' => $user_agent,
        ));
        if($flag == false) {
            throw new DataAccessException('Session was\'nt create');
        }
    }

    /**
     * @param Session $model
     * @throws DataAccessException
     */
    public function update(Model $model)
    {
        $id = $model->getId();
        $ip = $model->getIp();
        $user_agent = $model->getUserAgent();
        $con = $this->getPDO();
        $query = "update session set ip = hex(INET_ATON(:ip)), user_agent = :user_agent where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':ip' => $ip,
            ':user_agent' => $user_agent
        ));
        if($flag == false) {
            throw new DataAccessException('Session was\'nt update');
        }
    }

    public function read($id)
    {
        return $this->getById($id);
    }

    public function delete($id)
    {
        $con = $this->getPDO();
        $query = "delete from session where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        
    }

    public function getById($id) {
        $session = new Session();
        $con = $this->getPDO();
        $query = "select id, ip, user_agent from session where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        $result = $preparedQuery->fetch();
        $session->setId($id);
        $session->setIp($result['ip']);
        $session->setUserAgent($result['user_agent']);
        return $session;
    }

    /**
     * @param $ip
     * @return Session
     * @throws NoSuchObjectException
     */
    public function getByIp($ip) {
        $session = new Session();
        $con = $this->getPDO();
        $query = "select id from session where ip = hex(INET_ATON(:ip))";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':ip' => $ip
        ));
        $result = $preparedQuery->fetch();
        if($result == false) {
            throw new NoSuchObjectException('No such session');
        }
        $session->setId($result['id']);
        $session->setIp($ip);
        return $session;
    }
    /**
     * @return PDO
     */
    public function getPdo() :PDO
    {
        return $this->pdo;
    }



    public function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;
    }
}