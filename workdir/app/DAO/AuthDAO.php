<?php


namespace App\DAO;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Models\Auth;
use App\Models\EmailConfirm;
use App\Models\Model;
use PDO;

class AuthDAO implements DAO
{

    protected $pdo;

    /**
     * @param Auth $model
     */
    public function create(Model $model)
    {
        $token = $model->getToken();
        $userId = $model->getUserId();
        $session_id = $model->getSessionId();
        $con = $this->getPDO();
        $query = "insert into auth (user_id,session_id, token) values(:user_id, :session_id, :token)";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':session_id' => $session_id,
            ':user_id' => $userId,
            ':token' => $token
        ));
        if($flag == false) {
            throw new DataAccessException('Auth was\'nt create');
        }
    }

    /**
     * @param Auth $model
     * @throws DataAccessException
     */
    public function update(Model $model)
    {
        $id = $model->getId();
        $token = $model->getToken();
        $user_id = $model->getUserId();
        $session_id = $model->getSessionId();
        $con = $this->getPDO();
        $query = "update auth set session_id = :session_id,token = :token , user_id = :user_id where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id,
            ':user_id' => $user_id,
            ':session_id' => $session_id,
            ':token' => $token
        ));
        if($flag == false) {
            throw new DataAccessException('Auth Session was\'nt update');
        }
    }

    public function getById($id) {
        $auth = new Auth();
        $con = $this->getPDO();
        $query = "select user_id, session_id, token from auth where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':id' => $id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if($array == false) {
            throw new NoSuchObjectException("There is no such  Auth Session");
        }
        $user_id = $array['user_id'];
        $token = $array['token'];
        $session_id = $array['session_id'];
        $auth->setId($id);
        $auth->setUserId($user_id);
        $auth->setToken($token);
        $auth->setSessionId($session_id);
        return $auth;
    }

    public function getByUserId($user_id) {
        $auth = new Auth();
        $con = $this->getPDO();
        $query = "select id, session_id, token from auth where user_id = :user_id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':user_id' => $user_id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if($array == false) {
            throw new NoSuchObjectException("There is no such Auth Session");
        }
        $id = $array['id'];
        $token = $array['token'];
        $session_id = $array['session_id'];
        $auth->setId($id);
        $auth->setUserId($user_id);
        $auth->setToken($token);
        $auth->setSessionId($session_id);
        return $auth;
    }

    public function getBySessionId($session_id) {
        $auth = new Auth();
        $con = $this->getPDO();
        $query = "select id, user_id, token from auth where session_id = :session_id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $preparedQuery->execute(array(
            ':session_id' => $session_id
        ));
        $array = $preparedQuery->fetch(PDO::FETCH_ASSOC);
        if($array == false) {
            throw new NoSuchObjectException("There is no such  Auth Session");
        }
        $id = $array['id'];
        $token = $array['token'];
        $user_id = $array['user_id'];
        $auth->setId($id);
        $auth->setUserId($user_id);
        $auth->setToken($token);
        $auth->setSessionId($session_id);
        return $auth;
    }
    public function read($specification)
    {
        return $this->getById($specification);
    }

    /**
     * @param $id
     * @throws DataAccessException
     */
    public function delete($id)
    {
        $auth = $this->read($id);
        $con = $this->getPdo();
        $query = "delete from auth where id = :id";
        $preparedQuery = $con->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $flag = $preparedQuery->execute(array(
            ':id' => $id
        ));
        if($flag == false){
            throw new DataAccessException("Auth wasn't deleted");
        }
    }

    public function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;

    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }


}