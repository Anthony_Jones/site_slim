<?php


namespace App\Services;


use App\DAO\UserDAO;
use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Exceptions\UniqueEmailException;
use App\Exceptions\UniqueNicknameException;
use App\Models\EmailConfirm;
use App\Models\ResetToken;
use App\Models\User;
use Exception;

class UserService extends Service
{
    /**
     * @param $login
     * @param $password
     * @var $user User
     */
    public function login($login, $password)
    {
        $errors = null;
        try {
            $user = $this->container['UserDAO']->findByNickname($login);
        } catch (NoSuchObjectException $e) {
            $errors[] = 'No user with such nickname';
            return $errors;
        }
        if (hash('md5', $password) == $user->getPassword()) {
            return $errors;
        } else if(!$user->is_active()) {
            $errors[] = 'Confirm the email';
            return $errors;
        } else {
            $errors[] = 'Password is incorrect';
            return $errors;
        }
    }
    public function signup($email, $password, $nickname) {
        $errors = null;
        $newUser = new User();
        $newUser->setNickname($nickname);
        $newUser->setEmail($email);
        $newUser->setPassword(hash('md5', $password));

        /** @var string $rem_token  random hash string token*/
        $DAO = $this->container['UserDAO'];
        try {
            $DAO->create($newUser);
        } catch (DataAccessException $e) {
            $errors[] = $e->what();
        } catch (UniqueEmailException $e) {
            $errors[] = $e->what();
        } catch (UniqueNicknameException $e) {
            $errors[] = $e->what();
        }
        return $errors;
    }

    public function sendConfirmation($email, $nickname) {
        $errors = null;
        $emailConf = new EmailConfirm();
        $hash_key = hash('md5', $email.(string)time());
        $user = $this->container['UserDAO']->findByNickname($nickname)->getId();
        $emailConf->setHashKey($hash_key);
        $emailConf->setUserId($user);
        $is_sent = $this->container['MailService']->confirm($email, $hash_key, $user);
        if($is_sent) {
            $emailConfDAO = $this->container['EmailDAO'];
            try {
                $emailConfDAO->create($emailConf);
            }
            catch (DataAccessException $e){
                $errors[] ='Email confirmation was not created';
            }
        } else {
            $errors[] = 'Mail wasn\'t sent';
        }
        return $errors;
    }

    /**
     * @param $user_id
     * @param $token
     * @return mixed
     * @throws NoSuchObjectException
     */
    public function userBySession($user_id, $token) {
        $auth = $this->container['AuthDAO']->getByUserId($user_id);
        if($auth->getToken() == $token) {
            $user = $this->container['UserDAO']->getById($user_id);
            return $user;
        }  else {
            throw new NoSuchObjectException ('Token is incorrect');
        }
    }
    public function confirmation($key, $user_id) {
        $emailDao = $this->container['EmailDAO'];
        $errors = null;
        try {
            $emailConf = $emailDao->getByUserId($user_id);
        }catch (NoSuchObjectException $e) {
            $errors[] = 'Email was confirmed or does not exists';
            return $errors;
        }
        if($emailConf->getHashKey() == $key) {
            $userDAO =$this->container['UserDAO'];
            $user = $userDAO->getById($user_id);
            $user->setEmailVerifiedAt(date('Y-m-d h:m:s'));
            try {
                $userDAO->update($user);
            }catch (DataAccessException $e) {
                $errors[] = $e->what();
                return $errors;
            }
            try {
                $emailDao->delete($emailConf->getId());
            } catch (DataAccessException $e) {
                $errors[] = $e->what();
                return $errors;
            }
            return $errors;
        } else {
            return $errors;
        }
    }
}