<?php


namespace App\Services;

use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use App\Models\ResetToken;
use App\Models\User;
use Exception;

class ResetTokenService extends Service
{
    public function createResetToken($email)
    {
        $dao = $this->container['ResetTokenDAO'];
        $user = $this->container['UserDAO']->findByEmail($email);
        try {
            $rem_token = (string)bin2hex(random_bytes(16));
        } catch (Exception $e) {
            $errors[] = 'Create rem token error';
        }
        $token = new ResetToken();
        $token->setUserId($user->getId());
        $token->setResetToken($rem_token);
        $date = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
        $token->setExpiringDate(date('Y-m-d', $date));
        try  {
            $dao->create($token);
        } catch (DataAccessException $e) {
            $errors[] = $e->what();
        }
        return $errors;
    }

    public function userByToken($token) {
        $dao = $this->container['ResetTokenDAO'];
        $result = null;
        try {
            $token = $dao->getByToken($token);
        }catch (NoSuchObjectException $e) {
            $result['errors'][] = 'Token was unset';
            return $result;
        }
        if(date('Y-m-d') > $token->getExpiringDate()) {
            $result['errors'][] = 'Token is expired';
            $this->unsetTokens($token->getUserId());
            return $result;
        } else {
            $result['user'] = $this->container['UserDAO']->getById($token->getUserId());
            return $result;
        }
    }

    /**
     * @param $user_id
     * @param $password
     */
    public function reset($user_id, $password) { // Reset User Password
        $errors = null;
        $userDAO = $this->container['UserDAO'];
        try {
            $user = $userDAO->getById($user_id);

        } catch (NoSuchObjectException $e) {
            $errors[] = $e->what();
            return $errors;
        }
        $user->setPassword(hash('md5',$password));
        try {
            $userDAO->update($user);
        } catch (DataAccessException $e) {
            $errors[] = $e->what();
            return $errors;
        }
        return $errors;
    }
    public function unsetTokens($user_id){
        $dao = $this->container['ResetTokenDAO'];
        while(true) {
            try {
                $reset_token = $dao->getByUserId($user_id);
                $dao->delete($reset_token->getId());
            } catch(NoSuchObjectException $e) {
                break;
            }
        }
    }
}