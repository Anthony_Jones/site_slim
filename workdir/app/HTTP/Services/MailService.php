<?php


namespace App\Services;

use PHPMailer\PHPMailer\Exception;


class MailService extends Service
{
    private $from;
    private $fromName;
    private $username;
    private $password;
    private $host;
    private $port;


    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param mixed $fromName
     */
    public function setFromName($fromName): void
    {
        $this->fromName = $fromName;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host): void
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port): void
    {
        $this->port = $port;
    }


    public function reset($email, $key)
    {
        $mail = $this->container['mail'];
        try {
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = $this->getContainer()['mailConfig']['username'];                     // SMTP username
            $mail->Password = $this->getContainer()['mailConfig']['password'];                               // SMTP password
//            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('korzinkayablok@gmail.com', 'Mailer');
            $mail->addAddress($email);     // Add a recipient

            // Attachments
            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Password reset';
            $mail->Body = "
            <html lang=\"ru\">
            <head>
                <title>Reset Password</title>
            </head>
            <body>
                <div style=\"font-size:16px;line-height:1.4;font-family:Helvetica,Arial,sans-serif;text-align:left\" align=\"center\">
                <h1>Hello. It's reset password message</h1>
                <span>Click at the link bellow to reset your password</span>
                <a href=\"https://example.com/reset/$key\">https://example.com/reset/$key</a>
                <p>Дата: " . date('d.m.Y H:i:s') . "</p>
                </div>
            </body>
            </html>";
            $mail->AltBody = 'Reset password link: ' . 'https://example.com/reser/' . $key;
            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function confirm($email, $key, $user_id)
    {
        $mail = $this->container['mail'];
        try {
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                                   // Enable SMTP authentication
            $mail->Username = $this->getContainer()['mailConfig']['username'];                     // SMTP username
            $mail->Password = $this->getContainer()['mailConfig']['password'];                             // SMTP password
//            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('korzinkayablok@gmail.com', 'Mailer');
            $mail->addAddress($email);     // Add a recipient

            // Attachments
            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Email confirmation';
            $mail->Body = "
            <html lang=\"ru\">
            <head>
                <title>Подтверджение почты</title>
            </head>
            <body>
                <div style=\"font-size:16px;line-height:1.4;font-family:Helvetica,Arial,sans-serif;text-align:left\" align=\"center\">
                <h1>Hello. It's email confirmation message</h1>
                <span>Click at the link bellow to confirm your email</span>
                <a href=\"https://example.com/confirm/$key?token=$user_id\">https://example.com/confirm/$key\&token=$user_id</a>
                <p>Дата: " . date('d.m.Y H:i:s') . "</p>
                </div>
            </body>
            </html>";
            $mail->AltBody = 'Email confirm link: ' . 'https://example.com/confirm/' . $key . '&token=' . $user_id;

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}