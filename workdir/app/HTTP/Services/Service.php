<?php


namespace App\Services;


class Service
{
    protected $container;

    /**
     * Service constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }


}