<?php


namespace App\Controllers;


use App\Exceptions\DataAccessException;
use App\Exceptions\NoSuchObjectException;
use PDO;

class AdminController extends Controller
{
    public function users($request, $response, $args) {
        $users = null;
        /**
         * @var PDO pdo
         */
        $pdo = $this->container['PHPDataObjects']['mysqlWeb'];
        $authDao = $this->container['AuthDAO'];
        $query = "select id, nickname, email, email_verified_at, is_privileged from user";
        foreach ($pdo->query($query) as $row) {
            if($row['email_verified_at'] != null) {
                $active = 'Yes';
            } else {
                $active = 'No';
            }
            if($row['is_privileged']) {
                $privilege = 'Yes';
            } else {
                $privilege = 'No';
            }
            try {
                $authDao->getByUserId($row['id']);
                $is_logged = 'Yes';
            } catch (NoSuchObjectException $e) {
                $is_logged = 'No';
            }
            $user = ['id'=> $row['id'], $row['nickname'], $row['email'], $active, $privilege, $is_logged];
            $users[] = $user;
        }
        return $this->container['view']->render($response, '@admin/users.twig', ['users'=> $users]);
    }
    public function guests($request, $response, $args) {
        $guests = null;
        /**
         * @var PDO pdo
         */
        $pdo = $this->container['PHPDataObjects']['mysqlWeb'];
        $authDao = $this->container['AuthDAO'];
        $query = "select id, ip as ip, user_agent from session";
        foreach ($pdo->query($query) as $row) {
            try {
                $user_id = $authDao->getBySessionId($row['id'])->getUserId();
            } catch (NoSuchObjectException $e) {
                $user_id = 'Not logged in';
            }
            $ip = long2ip(hexdec((string)$row['ip']));
            $guest = [$row['id'], $ip, $row['user_agent'], $user_id];

            $guests[] = $guest;
        }
        return $this->container['view']->render($response, '@admin/guests.twig', ['guests'=> $guests]);
    }

    public function deleteUser($request, $response, $args) {
        if(isset($args['user_id'])) {
            try {

            try {
                $user = $this->container['UserDAO']->getById($args['user_id']);
            } catch (NoSuchObjectException $e) {
                $errors[] = 'No such user';
                return $response->withJson(400);
            }
            if($user->getEmailVerifiedAt() == null ) {
                $emailConfrimId = $this->container['EmailDAO']->getByUserId($args['user_id'])->getId();
                $this->container['EmailDAO']->delete($emailConfrimId);
            }
            try {
                $authId = $this->container['AuthDAO']->getByUserId($args['user_id'])->getId();
                $this->container['AuthDAO']->delete($authId);
                $this->container['ResetTokenService']->unsetTokens($args['user_id']);
            }catch (NoSuchObjectException $e) {}
            $this->container['UserDAO']->delete($args['user_id']);
            }catch (DataAccessException $e) {
                $errors[] = "Something went wrong";
                return $this->container['view']->render('home.twig', ['errors' => $errors]);
            }
            return $response->withJson(200);
        }else {
            return $response->withJson(400);
        }
    }
}