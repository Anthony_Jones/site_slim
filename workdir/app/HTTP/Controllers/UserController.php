<?php


namespace App\Controllers;


use App\Models\User;

class UserController extends Controller
{
    /**
     *
     * @param $request
     * @param $response
     * @param $args

     */
    public function profile($request, $response, $args)
    {
        $user = unserialize($_SESSION['user']);
        $nickname = $user->getNickname();
        $id = $user->getId();
        $email = $user->getEmail();
        if ($user->getIsPrivileged() == 1) {
            $admin = "You have admin roles";
        } else {
            $admin = null;
        };
        return $this->getContainer()['view']->render($response, 'profile.twig', [
            'nickname' => $nickname,
            'id' => $id,
            'email' => $email,
            'admin' => $admin
        ]);
    }

    public function logout($request, $response, $args)
    {
        if (isset($_SESSION['user'])) {
            if (isset($_COOKIE['token'])) {
                $user = unserialize($_SESSION['user']);
                $id = $user->getId();
                $auth = $this->container['AuthDAO']->getByUserId($id);
                $this->container['AuthDAO']->delete($auth->getId());
                setcookie('user_id', "", time() - 1);
                setcookie('token', "", time() - 1);
                unset($_COOKIE['user_id']);
                unset($_COOKIE['token']);
            }
            session_destroy();
        }
        return $response->withRedirect('/login', 302);
    }

}