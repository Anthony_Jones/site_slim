<?php


namespace App\Controllers;


class HomeController extends Controller
{

    public function index($request, $response, $args) {
        $session = $_COOKIE['CWSession'];
        return $this->container['view']->render($response, 'home.twig', ['session_id' => $session]);
    }

}