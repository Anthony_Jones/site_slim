<?php

namespace App\Controllers;


use Psr\Container\ContainerInterface;

class Controller
{

    protected $container;
    /**
     * Controller constructor.
     * @param $container ContainerInterface DependencyContainer
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }


}