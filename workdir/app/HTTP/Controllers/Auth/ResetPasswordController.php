<?php

namespace App\Controllers;


use App\Exceptions\DataAccessException;
use App\Models\User;
use Psr\Http\Message\RequestInterface;
use Respect\Validation\Validator;

class ResetPasswordController extends Controller
{

    public function reset(RequestInterface $request, $response, $args)
    {
        if ($request->isGet()) {
            $nameKey = $this->container['csrf']->getTokenNameKey();
            $valueKey = $this->container['csrf']->getTokenValueKey();
            $name = $request->getAttribute($nameKey);
            $value = $request->getAttribute($valueKey);
            return $this->container['view']->render($response, 'reset.twig', [
                'name' => $name,
                'value' => $value,
                'nameKey' => $nameKey,
                'valueKey' => $valueKey
            ]); //show Reset view
        } else if ($request->isPost()) { // Create Reset token
            if (false === $request->getAttribute('csrf_status')) {
                $errors = "Failure while passing CSRF check. Reload page";
                return $this->container['view']->render($response, 'layout.twig', ['errors' => $errors]);
            } else {
                $v = $this->container['validator'];
                $email = htmlspecialchars(trim($_POST['email']));
                if ($v::email()->validate($email) != true) {
                    $errors[] = 'Invalid email';
                    return $errors;
                }
                $resetService = $this->container['ResetTokenService'];
                try {
                    $errors = $resetService->createResetToken($email);
                } catch (DataAccessException $e) {
                    $errors[] = $e->what();
                }
                if ($errors[0] != null) {
                    return $this->container['view']->render($response, 'layout.twig', ['errors' => $errors]);
                }
                $mail = $this->container['MailService'];
                $resetTokenDAO = $this->container['ResetTokenDAO'];
                $user = $this->container['UserDAO']->findByEmail($email);;
                $key = $resetTokenDAO->getByUserId($user->getId())->getResetToken();
                if ($user->is_active()) {
                    $mail->reset($email, $key);
                    return $this->container['view']->render($response, 'confirm.twig');
                } else {
                    return $this->container['view']->render($response, 'layout.twig', ['errors' => $errors]);
                }
            }
        } else {
            return $this->container['view']->render($response, 'home.twig');
        }
    }

    /**
     * @param RequestInterface $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function resetAPI(RequestInterface $request, $response, $args)
    { //Reset user password
        $token = $args['token'];
        if ($request->isGet()) {
            $nameKey = $this->container['csrf']->getTokenNameKey();
            $valueKey = $this->container['csrf']->getTokenValueKey();
            $name = $request->getAttribute($nameKey);
            $value = $request->getAttribute($valueKey);
            return $this->container['view']->render($response, 'resetPassword.twig', [
                'token' => $token,
                'name' => $name,
                'value' => $value,
                'nameKey' => $nameKey,
                'valueKey' => $valueKey
            ]);
        } else if ($request->isPost()) {
            if (false === $request->getAttribute('csrf_status')) {
                $errors[] = "Failure while passing CSRF check. Reload page";
                return $this->container['view']->render($response, 'layout.twig', ['errors' => $errors]);
            }
            $errors = null;
            /**
             * $v @var Validator
             */
            $v = $this->container['validator'];
            $resetService = $this->container['ResetTokenService'];
            /**
             * $userByToken @var User
             */
            $userByToken = $resetService->userByToken($token);
            if(isset($userByToken['errors'])) {
                $errors[] = $userByToken['errors'][0];
                return $this->container['view']->render($response, 'resetPassword.twig', [
                    'errors' => $errors,
                ]);
            }
            $oldPassword = hash('md5',htmlspecialchars(trim($_POST['oldPassword'])));
            if($v::equals($oldPassword)->validate($userByToken['user']->getPassword()) != true) {
                $errors[] = 'Old password is incorrect';
                return $this->container['view']->render($response, 'resetPassword.twig', [
                    'errors' => $errors,
                    'token' => $token
                ]);
            }
            $password = htmlspecialchars(trim($_POST['password']));
            if ($v::noWhitespace()->length(7, 20)->validate($password) != true) {
                $errors[] = 'Invalid password. Length from 7 to 20 chars. Without whitespaces';
                return $this->container['view']->render($response, 'resetPassword.twig', [
                    'errors' => $errors,
                    'token' => $token
                ]);
            }
            $repeat = htmlspecialchars(trim($_POST['repeat']));
            if ($v::equals($password)->validate($repeat) != true) {
                $errors[] = 'Passwords does not match';
                return $this->container['view']->render($response, 'resetPassword.twig', [
                    'errors' => $errors,
                    'token' => $token
                ]);
            }


            if ($errors != null) {
                return $this->container['view']->render($response, 'resetPassword.twig', [
                    'errors' => $errors,
                    'token' => $token
                ]);
            } else {
                $userId = $userByToken['user']->getId();
                $errors = $resetService->reset($userId, $password);
                if ($errors == null) {
                    $resetService->unsetTokens($userId);
                }
            }
            return $response->withRedirect('/login', 200);
        } else {
            return $this->container['view']->render($response, 'home.twig');
        }
    }
}