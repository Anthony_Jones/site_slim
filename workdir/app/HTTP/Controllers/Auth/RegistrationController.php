<?php


namespace App\Controllers;


use App\Models\Auth;
use Respect\Validation\Validator;

class RegistrationController extends Controller
{
    public function signup($request, $response, $args)
    {
        if ($request->isGet()) {
            $nameKey = $this->container['csrf']->getTokenNameKey();
            $valueKey = $this->container['csrf']->getTokenValueKey();
            $name = $request->getAttribute($nameKey);
            $value = $request->getAttribute($valueKey);
            return $this->container['view']->render($response, 'registration.twig', [
                'name' => $name,
                'value' => $value,
                'nameKey' => $nameKey,
                'valueKey' => $valueKey
            ]);
        } else if ($request->isPost()) {
            $errors = null;
            if (false === $request->getAttribute('csrf_status')) {
                $errors[] = "Failure while passing CSRF check. Reload page";
                return $this->container['view']->render($response, 'registration.twig', ['errors' => $errors]);
            } else {
                /**
                 * $v @var Validator
                 */
                $v = $this->container['validator'];
                $email = htmlspecialchars(trim($_POST['email']));
                if ($v::email()->validate($email) != true) {
                    $errors[] = 'Invalid email';
                }
                $password = htmlspecialchars(trim($_POST['password']));
                if ($v::noWhitespace()->length(7, 20)->validate($password) != true) {
                    $errors[] = 'Invalid password. Length from 7 to 20 chars';
                }

                $repeat = htmlspecialchars(trim($_POST['repeat']));
                if ($v::equals($password)->validate($repeat) != true) {
                    $errors[] = 'Passwords does not match';
                }
                $nickname = htmlspecialchars(trim($_POST['nickname']));
                if ($v::noWhitespace()->length(1, 20)->validate($nickname) != true) {
                    $errors[] = 'Invalid nickname. Length from 1 to 12 chars';
                }
                if ($errors == null) {
                    $errors = $this->container['UserService']->signup($email, $password, $nickname);
                    if ($errors[0] == null) {
                        $errors = $this->container['UserService']->sendConfirmation($email, $nickname);
                    }
                }
                if ($errors[0] != null) {
                    return $this->container['view']->render($response, 'registration.twig', ['errors' => $errors]);
                } else {
                    $session_id = $_COOKIE['CWSession'];
                    return $this->container['view']->render($response, 'home.twig', ['session_id' => $session_id]);
                }
            }
        } else {
            return $this->container['view']->render($response, 'home.twig');
        }
    }


    /**
     * Confirm email
     * @param $request
     * @param $response
     * @param $args
     */
    public function confirm($request, $response, $args)
    {
        if (isset($args['key']) and isset($args['user_id'])) {
            $errors = $this->container['UserService']->confirmation($args['key'], $args['user_id']);
            if($errors === null) {
                $user = $this->getContainer()['UserDAO']->getById($args['user_id']);
                $auth = new Auth();
                $auth->setSessionId($_COOKIE['CWSession']);
                $auth->setUserId($user->getId());
                $auth->setToken((string)bin2hex(random_bytes(16)));
                $this->container['AuthDAO']->create($auth);
                setcookie('token', $auth->getToken(), time() + 2592000);
                setcookie('user_id', $args['user_id'], time() + 2592000);
                $_SESSION['user'] = serialize($user);
            }
            return $this->container['view']->render($response, 'home.twig', ['errors' => $errors]);
        } else {
            return $response->withRedirect('/confirm', 202);
        }
    }
}