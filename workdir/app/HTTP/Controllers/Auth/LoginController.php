<?php


namespace App\Controllers;


use App\Models\Auth;
use App\Services\UserService;
use Psr\Http\Message\ResponseInterface;

class LoginController extends Controller
{


    public function login($request,ResponseInterface $response, $args)
    {
        if ($request->isGet()) {
            if(isset($_SESSION['user'])) {
                return $response->withRedirect('/profile', 302);
            }
            $nameKey = $this->container['csrf']->getTokenNameKey();
            $valueKey = $this->container['csrf']->getTokenValueKey();
            $name = $request->getAttribute($nameKey);
            $value = $request->getAttribute($valueKey);
            return $this->container['view']->render($response, 'login.twig', [
                'name' => $name,
                'value' => $value,
                'nameKey' => $nameKey,
                'valueKey' => $valueKey
            ]);
        } else if ($request->isPost()) {
            $errors = null;
            if (false === $request->getAttribute('csrf_status')) {
                $errors[] = "Failure while passing CSRF check. Reload page";
                return $this->container['view']->render($response, 'login.twig', ['errors' => $errors]);
            } else {
                // successfully passed CSRF check
                $v = $this->container['validator'];
                $password = htmlspecialchars(trim($_POST['password']));
                if ($v::noWhitespace()->length(6, 20)->validate($password) != true) {
                    $errors[] = 'Invalid password';
                }
                $nickname = htmlspecialchars(trim($_POST['nickname']));
                if ($v::noWhitespace()->length(1, 12)->validate($nickname) != true) {
                    $errors[] = 'Invalid nickname';
                }
                if ($errors != null) {
                    return $this->container['view']->render($response, 'login.twig', ['errors' => $errors]);
                }
                $userService = new UserService($this->container);
                $errors = $userService->login($nickname, $password);
                if ($errors != null) {
                    return $this->container['view']->render($response, 'login.twig', ['errors' => $errors]);
                } else {
                    $user = $this->getContainer()['UserDAO']->findByNickname($nickname);
                    $auth = new Auth();
                    $auth->setSessionId($_COOKIE['CWSession']);
                    $auth->setUserId($user->getId());
                    $auth->setToken((string)bin2hex(random_bytes(16)));
                    $this->container['AuthDAO']->create($auth);
                    setcookie('token', $auth->getToken(), time() + 2592000);
                    setcookie('user_id', $user->getId(), time() + 2592000);
                    $_SESSION['user'] = serialize($user);
                    return $this->container['view']->render($response, 'home.twig', ['session_id' => $_COOKIE['CWSession']]);
                }
            }
        } else {
            return $this->container['view']->render($response, 'home.twig');
        }
    }

}