<?php


namespace App\Models;


class ResetToken extends Model
{
    protected $id;
    protected $user_id;
    protected $reset_token;
    protected $expiring_date;

    /**
     * @return mixed
     */
    public function getExpiringDate()
    {
        return $this->expiring_date;
    }

    /**
     * @param mixed $expiring_date
     */
    public function setExpiringDate($expiring_date): void
    {
        $this->expiring_date = $expiring_date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getResetToken()
    {
        return $this->reset_token;
    }

    /**
     * @param mixed $reset_token
     */
    public function setResetToken($reset_token): void
    {
        $this->reset_token = $reset_token;
    }


}