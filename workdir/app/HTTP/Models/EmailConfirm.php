<?php


namespace App\Models;


class EmailConfirm extends Model
{
    protected $id;
    protected $user_id;
    protected $hash_key;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getHashKey()
    {
        return $this->hash_key;
    }

    /**
     * @param mixed $hash_key
     */
    public function setHashKey($hash_key): void
    {
        $this->hash_key = $hash_key;
    }


}