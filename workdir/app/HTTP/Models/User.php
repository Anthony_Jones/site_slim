<?php
namespace App\Models;


class User extends Model
{
    protected $id;
    protected $email;
    protected $password;
    protected $nickname;
    protected $email_verified_at;
    protected $is_privileged;

    /**
     * @return mixed
     */
    public function getIsPrivileged()
    {
        return $this->is_privileged;
    }

    /**
     * @param mixed $is_privileged
     */
    public function setIsPrivileged($is_privileged): void
    {
        $this->is_privileged = $is_privileged;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param mixed $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }


    /**
     * @return mixed
     */
    public function getEmailVerifiedAt()
    {
        return $this->email_verified_at;
    }

    /**
     * @param mixed $email_verified_at
     */
    public function setEmailVerifiedAt($email_verified_at)
    {
        $this->email_verified_at = $email_verified_at;
    }


    public function is_active() : bool
    {
        return $this->email_verified_at != null;
    }

}