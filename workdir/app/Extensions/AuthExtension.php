<?php

namespace App\Extension;

use Slim\Views\TwigExtension;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\NodeVisitor\NodeVisitorInterface;
use Twig\TokenParser\TokenParserInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

class AuthExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('HideFromUser', function () {
                if (isset($_SESSION['user'])) {
                    return 'style=display:none';
                }
                return '';
            }),
            new TwigFunction('HideFromGuest', function () {
                if (!isset($_SESSION['user'])) {
                    return 'style=display:none';
                }
                return '';
            }),
            new TwigFunction('HideFromNonAdmin', function () {
                if (isset($_SESSION['user'])) {
                    if(unserialize($_SESSION['user'])->getIsPrivileged() == 0) {
                        return 'style=display:none';
                    }
                    return'';
                }else {
                    return 'style=display:none';
                }
            }),
        ];
    }



}
