<?php


namespace App\Exceptions;


use Exception;

class UniqueNicknameException extends Exception
{
    protected $message;

    /**
     * UniqueNicknameException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
        $this->message = $message;
    }

    public function what() {
        return $this->message;
    }
}