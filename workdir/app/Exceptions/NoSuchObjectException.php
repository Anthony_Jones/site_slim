<?php


namespace App\Exceptions;


use Exception;

class NoSuchObjectException extends Exception
{
    protected $message;

    /**
     * NoSuchObjectException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
        $this->message = $message;
    }

    public function what() {
        return $this->message;
    }


}