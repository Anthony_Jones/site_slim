<?php


namespace App\Exceptions;

use Exception;

/**
 * Class DataAccessException
 * Throwing when PDO query wasn't executed right while parameters was right
 * @package App\Exceptions
 */
class DataAccessException extends Exception
{
    protected $message;

    /**
     * DataAccessException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
        $this->message = $message;
    }

    public function what() {
        return $this->message;
    }
}