<?php


namespace App\Exceptions;


use Exception;

class UniqueEmailException extends Exception
{

    protected $message;

    /**
     * UniqueEmailException constructor.
     * @param $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
        $this->message = $message;
    }

    public function what() {
        return $this->message;
    }
}