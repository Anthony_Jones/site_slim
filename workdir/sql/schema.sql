use web;
drop table if exists email_confirmation;
drop table if exists auth;
drop table if exists reset_password;
drop table if exists user;
drop table if exists session;

create table user
(
    id                bigint auto_increment
        primary key,
    nickname          varchar(255)         not null,
    email             varchar(255)         not null,
    password          varchar(32)          not null,
    email_verified_at datetime             null,
    is_privileged     tinyint(1) default 0 null,
    constraint email_uk
        unique (email),
    constraint user_uk
        unique (nickname, email)
);

create table session
(
    id         bigint auto_increment
        primary key,
    ip         varchar(8)   not null,
    user_agent varchar(255) not null,
    constraint session_user_fk
        unique (ip, user_agent)
);

create table auth
(
    id         int auto_increment
        primary key,
    user_id    bigint      null,
    session_id bigint      not null,
    token      varchar(32) not null,
    constraint auth_session_id_fk
        foreign key (session_id) references session (id),
    constraint auth_user_fk
        foreign key (user_id) references user (id)
);

create table email_confirmation
(
    id       int auto_increment
        primary key,
    user_id  bigint      null,
    hash_key varchar(32) not null,
    constraint email_user_fk
        foreign key (user_id) references user (id)
);


create table reset_token
(
    id            int auto_increment
        primary key,
    user_id       bigint      null,
    reset_token   varchar(32) not null,
    expiring_date varchar(32) not null,
    constraint user_id
        unique (user_id),
    constraint reset_token_user_id_fk
        foreign key (user_id) references user (id)
);


