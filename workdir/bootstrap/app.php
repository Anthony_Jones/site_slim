<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\SessionMiddleware;
use App \Middleware\TrailingSlash;
use Slim\App;
use Slim\Container;

require_once '../config/app.php';


$container = new Container();

$app = new App([$config,$container]);

require_once '../config/mail.php';

$app->getContainer()['mail'] = $mailConfig;

require_once 'dependencies.php';

$app->add(new SessionMiddleware($app->getContainer()));
$app->add($app->getContainer()['csrf']);
$app->add(new TrailingSlash());

