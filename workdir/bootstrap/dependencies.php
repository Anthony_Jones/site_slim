<?php

use App\Controllers\AdminController;
use App\Controllers\HomeController;
use App\Controllers\LoginController;
use App\Controllers\ResetPasswordController;
use App\Controllers\UserController;
use App\Controllers\RegistrationController;

use App\DAO\AuthDAO;
use App\DAO\EmailConfirmDAO;
use App\DAO\ResetTokenDAO;
use App\DAO\SessionDAO;
use App\DAO\UserDAO;

use App\Extension\AuthExtension;

use App\Services\MailService;
use App\Services\ResetTokenService;
use App\Services\UserService;



use PHPMailer\PHPMailer\PHPMailer;

use Respect\Validation\Validator;

use Slim\Csrf\Guard;
use Slim\Http\Environment;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

/**
 * Dependency container
 */
$container = $app->getContainer();


$container['csrf'] = function ($container) {
    $guard = new Guard;
    $guard->setFailureCallable(function ($request, $response, $next) {
        $request = $request->withAttribute("csrf_status", false);
        return $next($request, $response);
    });
    return $guard;
};

//$container['settings']['template'] = [];
$container['HomeController'] = function ($container) {return new HomeController($container);};
$container['LoginController'] = function ($container) {return new LoginController($container);};
$container['ResetPasswordController'] = function ($container) {return new ResetPasswordController($container);};
$container['UserController'] = function ($container) {return new UserController($container);};
$container['RegistrationController'] = function ($container) {return new RegistrationController($container);};
$container['AdminController'] = function ($container) {return new AdminController($container);};

$container['validator'] = function () {return new Validator();};

$container['view'] = function ($container) {
    $basePath = '../resources/views/';
    $view = new Twig([$basePath.'main','admin'=> $basePath.'admin']);
    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));
    $view->addExtension(new TwigExtension($router, $uri));
    $view->getEnvironment()->addExtension(new AuthExtension());
    return $view;
};

$container['mail'] = function () {return new PHPMailer();};

$container['MailService'] = function ($container) {return new MailService($container);};
$container['UserService'] = function ($container) {return new UserService($container);};
$container['ResetTokenService'] = function ($container) {return new ResetTokenService($container);};

require_once 'pdo.php';

$container['PHPDataObjects'] = $PHPDataObjects;

$container['UserDAO'] = function ($container) {return new UserDAO($container['PHPDataObjects']['mysqlWeb']);};
$container['SessionDAO'] = function ($container) {return new SessionDAO($container['PHPDataObjects']['mysqlWeb']);};
$container['EmailDAO'] = function ($container) {return new EmailConfirmDAO($container['PHPDataObjects']['mysqlWeb']);};
$container['ResetTokenDAO'] = function ($container) {return new ResetTokenDAO($container['PHPDataObjects']['mysqlWeb']);};
$container['AuthDAO'] = function ($container) {return new AuthDAO($container['PHPDataObjects']['mysqlWeb']);};

