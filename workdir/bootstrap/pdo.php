<?php

require_once '../config/database.php';

$PHPDataObjects = [];

foreach ($connectionStrings as $conString) {
    $driver = $conString['driver'];
    $host = $conString['host'];
    $port = $conString['port'];
    $dbname = $conString['dbname'];
    $user = $conString['user'];
    $password = $conString['password'];

    $Connection = $driver.':host='.$host;
    if(!empty($port)) {
        $Connection = $Connection.';port='.$port;
    }
    $Connection = $Connection. ';dbname='.$dbname;

    try {
        $PHPDataObjects[$driver.ucfirst($dbname)] =
            new PDO($Connection, $user, $password, array(
                PDO::ATTR_PERSISTENT => true));
    } catch (PDOException $e) {
        print "Error!:" . $e->getMessage() . "<br/>";
    }
}

