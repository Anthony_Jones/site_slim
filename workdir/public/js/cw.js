let Table; // DataTable

$(document).ready(function () {
    Table = $('#example').DataTable();
});

function deleteUser(id) {
    $.ajax('/delete/'+id, {'method':'delete' , 'async':'false'}).done(function () {
        alert("Success");
        let rowId = Table.row($('#'+id)).index();
        Table.row(rowId).remove().draw();
    }).fail(function () {
        $('.card').before("<div class='alert alert-danger' role='alert'>Fail deleting</div>");
    })
}