<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\RoleAccessMiddleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->group('/' ,function ($app) {
    $app->get('[home]', function (Request $request, Response $response, $args) {
        return $this->HomeController->index($request, $response, $args);
    });
});

$app->map(['GET', 'POST'], '/signup', function (Request $request, Response $response, $args) {
    return $this->RegistrationController->signup($request, $response, $args);
});
$app->map(['GET', 'POST'], '/login', function (Request $request, Response $response, $args) {
    return $this->LoginController->login($request, $response, $args);
});
$app->group('/reset' ,function ($app) {
    $app->map(['GET', 'POST'], '', function (Request $request, Response $response, $args) {
        return $this->ResetPasswordController->reset($request, $response, $args);
    });

    $app->map(['GET', 'POST'], '/{token}', function (Request $request, Response $response, $args) {
        return $this->ResetPasswordController->resetAPI($request, $response, $args);
    });
});

$app->get('/logout', function (Request $request, Response $response, $args) {
    return $this->UserController->logout($request, $response, $args);
});

$app->group('/profile' ,function ($app) {
    $app->get('', function (Request $request, Response $response, $args) {
        return $this->UserController->profile($request, $response, $args);
    });
})->add(new AuthMiddleware($app->getContainer()));

$app->delete('/delete/{user_id}',function (Request $request, Response $response, $args) {
    return $this->AdminController->deleteUser($request, $response, $args);
});

$app->group('/confirm', function ($app) {
   $app->get('[/{key}'.'&token={user_id}]', function (Request $request, Response $response, $args) {
       return $this->RegistrationController->confirm($request, $response, $args);
   });
});

$app->group('/admin', function ($app){
    $app->get('/users', function (Request $request, Response $response, $args) {
        return $this->AdminController->users($request, $response, $args);
    });
    $app->get('/guests', function (Request $request, Response $response, $args) {
        return $this->AdminController->guests($request, $response, $args);
    });
})->add(new RoleAccessMiddleware())->add(new AuthMiddleware($app->getContainer()));

$app->get('/*', function (Request $request, Response $response, $args) {
   return $this->notFoundHandler;
});